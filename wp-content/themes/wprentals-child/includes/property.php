<?php

function details_estate_box($post) {
    global $post;
    wp_nonce_field(plugin_basename(__FILE__), 'estate_property_noncename');
    $week_days=array(
    '0'=>esc_html__('All','wpestate'),
    '1'=>esc_html__('Monday','wpestate'), 
    '2'=>esc_html__('Tuesday','wpestate'),
    '3'=>esc_html__('Wednesday','wpestate'),
    '4'=>esc_html__('Thursday','wpestate'),
    '5'=>esc_html__('Friday','wpestate'),
    '6'=>esc_html__('Saturday','wpestate'),
    '7'=>esc_html__('Sunday','wpestate')
 
    );
    
   
    $options_array=array(
            0   =>  esc_html__('Single Fee','wpestate'),
            1   =>  esc_html__('Per Night','wpestate'),
            2   =>  esc_html__('Per Guest','wpestate'),
            3   =>  esc_html__('Per Night per Guest','wpestate')
        );
    
    $mypost             =   $post->ID;
    
    $checkin_change_over            =   floatval   ( get_post_meta($mypost, 'checkin_change_over', true) );  
    $checkin_checkout_change_over   =   floatval   ( get_post_meta($mypost, 'checkin_checkout_change_over', true) );  
    $city_fee_per_day               =   floatval   ( get_post_meta($mypost, 'city_fee_per_day', true) );  
    $cleaning_fee_per_day           =   floatval   ( get_post_meta($mypost, 'cleaning_fee_per_day', true) );  
    $city_fee_percent               =   floatval   ( get_post_meta($mypost, 'city_fee_percent', true) ); 
    
    print'            
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr >
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="property_price">'.esc_html__( 'Price: ','wpestate').'</label><br />
            <input type="text" id="property_price" size="40" name="property_price" value="' . intval(get_post_meta($mypost, 'property_price', true)) . '">
            </p>
        </td>';

        print'
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="property_price_before_label">'.esc_html__( 'Before Price Label: ','wpestate').'</label><br />
            <input type="text" id="property_price_before_label" size="40" name="property_price_before_label" value="' . esc_html(get_post_meta($mypost, 'property_price_before_label', true)) . '">
            </p>
        </td>';
   
        print'
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="property_price_after_label">'.esc_html__( 'After Price Label: ','wpestate').'</label><br />
            <input type="text" id="property_price_after_label" size="40" name="property_price_after_label" value="' . esc_html(get_post_meta($mypost, 'property_price_after_label', true)) . '">
            </p>
        </td>';
    
    print'
    </tr>  
    
    <tr >';

        print'
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="security_deposit">'.esc_html__( 'Security Deposit ','wpestate').'</label><br />
            <input type="text" id="security_deposit" size="40" name="security_deposit" value="' . esc_html(get_post_meta($mypost, 'security_deposit', true)) . '">
            </p>
        </td>';
    
    print'
    </tr>  
   
    <tr >
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="early_bird_percent">'.esc_html__( 'Early Bird Discount value- in % from the price per night','wpestate').'</label><br />
            <input type="text" id="early_bird_percent" size="40" name="early_bird_percent" value="' . esc_html(get_post_meta($mypost, 'early_bird_percent', true)) . '">
            </p>
        </td>';
        
        print'
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="early_bird_days">'.esc_html__( 'Early Bird Discount no of days ','wpestate').'</label><br />
            <input type="text" id="early_bird_days" size="40" name="early_bird_days" value="' . esc_html(get_post_meta($mypost, 'early_bird_days', true)) . '">
            </p>
        </td>';

   
    print'
    </tr>
 
    <tr>
        <td valign="top" align="left">
        '.esc_html__('These options do not work together - choose only one and leave the other one on "All"','wpestate').'
        </td>
    </tr>
      

      <tr>
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="checkin_change_over">'. esc_html__('Allow only bookings starting with the check in on:','wpestate').'</label></br>
            <select id="checkin_change_over" name="checkin_change_over" class="select-submit2">';
              
                foreach($week_days as $key=>$value){
                    print '   <option value="'.$key.'"';
                    if( $key==$checkin_change_over){
                        print ' selected="selected" ';
                    }
                    print '>'.$value.'</option>';
                }
            print'    
            </select>
            </p>
        </td>

        <td width="33%" valign="top" align="left">
            <p class="meta-options"> 
            <label for="checkin_checkout_change_over">'. esc_html__('Allow only bookings with the check in/check out on: ','wpestate').'</label></br>
            <select id="checkin_checkout_change_over" name="checkin_checkout_change_over" class="select-submit2">';
               
                foreach($week_days as $key=>$value){
                   print '   <option value="'.$key.'"';
                    if( $key==$checkin_checkout_change_over){
                        print ' selected="selected" ';
                    }
                    print '>'.$value.'</option>';
                }
              print '
            </p>
        </td>
    </tr>

    <tr>  
        <td width="33%" valign="top" align="left">
            <p class="meta-options">
            <label for="property_size">'.esc_html__( 'Size: ','wpestate').'</label><br />
            <select id="property_size" name="property_size">
                <option value="s" '. (get_post_meta($mypost, 'property_size', true) == 's' ? 'selected' : '') . '>' . esc_html__( 'S - about 5’x5’ perfect for boxes, household','wpestate') .'</option>
                <option value="m" '. (get_post_meta($mypost, 'property_size', true) == 'm' ? 'selected' : '') . '>' . esc_html__( 'M - about 10’x10’ perfect for bikes, furniture','wpestate') .'</option>
                <option value="l" '. (get_post_meta($mypost, 'property_size', true) == 'l' ? 'selected' : '') . '>' . esc_html__( 'L - about 12’x15’ perfect for vehicle, boat','wpestate') .'</option>
                <option value="xl" '. (get_post_meta($mypost, 'property_size', true) == 'xl' ? 'selected' : '') . '>' . esc_html__( 'XL - larger than 12’x15’ perfect for XL stuff') .'</option>
            </select>
            </p>
        </td>
    </tr>
    <tr>';
     
     $option_video='';
     $video_values = array('vimeo', 'youtube');
     $video_type = get_post_meta($mypost, 'embed_video_type', true);

     foreach ($video_values as $value) {
         $option_video.='<option value="' . $value . '"';
         if ($value == $video_type) {
             $option_video.='selected="selected"';
         }
         $option_video.='>' . $value . '</option>';
     }
     
     
    print'
    <td valign="top" align="left">
        <p class="meta-options">
        <label for="embed_video_type">'.esc_html__( 'Video from ','wpestate').'</label><br />
        <select id="embed_video_type" name="embed_video_type" style="width: 237px;">
                ' . $option_video . '
        </select>       
        </p>
    </td>';

  
    print'
    <td valign="top" align="left">
      <p class="meta-options">     
      <label for="embed_video_id">'.esc_html__( 'Video id: ','wpestate').'</label> <br />
        <input type="text" id="embed_video_id" name="embed_video_id" size="40" value="'.esc_html( get_post_meta($mypost, 'embed_video_id', true) ).'">
      </p>
    </td>
    </tr>
    </table>';
}