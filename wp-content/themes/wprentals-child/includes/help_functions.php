<?php

function wpestate_booking_price($curent_guest_no,$invoice_id, $property_id, $from_date, $to_date,$bookid='',$extra_options_array='',$manual_expenses=''){
    
    

    $price_array                =   wpml_custom_price_adjust($property_id);    
    $mega                       =   wpml_mega_details_adjust($property_id);
 
    $cleaning_fee_per_day       =   floatval   ( get_post_meta($property_id,  'cleaning_fee_per_day', true) );
    $city_fee_per_day           =   floatval   ( get_post_meta($property_id, 'city_fee_per_day', true) );
    $price_per_weekeend         =   floatval   ( get_post_meta($property_id, 'price_per_weekeend', true) );  
    $setup_weekend_status       =   esc_html ( get_option('wp_estate_setup_weekend','') );
    $include_expeses            =   esc_html ( get_option('wp_estate_include_expenses','') );
    $booking_from_date          =   $from_date;
    $booking_to_date            =   $to_date;
    $total_guests               =   floatval(get_post_meta($bookid, 'booking_guests', true));
    
    $numberDays=1;
    if( $invoice_id == 0){
        $price_per_day              =   floatval(get_post_meta($property_id, 'property_price', true));
        $week_price                 =   floatval(get_post_meta($property_id, 'property_price_per_week', true));
        $month_price                =   floatval(get_post_meta($property_id, 'property_price_per_month', true));
        $cleaning_fee               =   floatval(get_post_meta($property_id, 'cleaning_fee', true));
        $city_fee                   =   floatval(get_post_meta($property_id, 'city_fee', true));
        $cleaning_fee_per_day       =   floatval(get_post_meta($property_id, 'cleaning_fee_per_day', true));
        $city_fee_per_day           =   floatval(get_post_meta($property_id, 'city_fee_per_day', true));
        $city_fee_percent           =   floatval(get_post_meta($property_id, 'city_fee_percent', true));
        $security_deposit           =   floatval(get_post_meta($property_id, 'security_deposit', true));
        $early_bird_percent         =   floatval(get_post_meta($property_id, 'early_bird_percent', true));
        $early_bird_days            =   floatval(get_post_meta($property_id, 'early_bird_days', true));
       
    }else{
        $price_per_day              =   floatval(get_post_meta($invoice_id, 'default_price', true));
        $week_price                 =   floatval(get_post_meta($invoice_id, 'week_price', true));
        $month_price                =   floatval(get_post_meta($invoice_id, 'month_price', true));
        $cleaning_fee               =   floatval(get_post_meta($invoice_id, 'cleaning_fee', true));
        $city_fee                   =   floatval(get_post_meta($invoice_id, 'city_fee', true));
        $cleaning_fee_per_day       =   floatval(get_post_meta($invoice_id, 'cleaning_fee_per_day', true));
        $city_fee_per_day           =   floatval(get_post_meta($invoice_id, 'city_fee_per_day', true));
        $city_fee_percent           =   floatval(get_post_meta($invoice_id, 'city_fee_percent', true));
        $security_deposit           =   floatval(get_post_meta($invoice_id, 'security_deposit', true));
        $early_bird_percent         =   floatval(get_post_meta($invoice_id, 'early_bird_percent', true));
        $early_bird_days            =   floatval(get_post_meta($invoice_id, 'early_bird_days', true));
    }
    
//  print ' before '.$booking_from_date;
//   //before 2018-04-25 after 2018-04-25
 // $booking_from_date   = wpestate_convert_dateformat($booking_from_date);
//  print ' after '.$booking_from_date;
//     $booking_to_date    = wpestate_convert_dateformat($booking_to_date);
    
    $from_date      =   new DateTime($booking_from_date);
    $from_date_unix =   $from_date->getTimestamp();
    $from_date_discount= $from_date->getTimestamp();
    $to_date        =   new DateTime($booking_to_date);
    $to_date_unix   =   $to_date->getTimestamp();
    $total_price    =   0;
    $inter_price    =   0;
    $has_custom     =   0;
    $usable_price   =   0;
    $has_wkend_price=   0;
    $cover_weekend  =   0;
    $custom_period_quest = 0;

    $custom_price_array =   array();        
    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $count_days         =   $timeDiff/86400;  // 86400 seconds in one day
    $count_days         =   intval($count_days);
    
    //check extra price per guest
    ///////////////////////////////////////////////////////////////////////////
    $extra_price_per_guest          =   floatval   ( get_post_meta($property_id, 'extra_price_per_guest', true) );  
    $price_per_guest_from_one       =   floatval   ( get_post_meta($property_id, 'price_per_guest_from_one', true) );
    $overload_guest                 =   floatval   ( get_post_meta($property_id, 'overload_guest', true) );
    $guestnumber                    =   floatval   ( get_post_meta($property_id, 'guest_no', true) );
    $has_guest_overload             =   0;
    $total_extra_price_per_guest    =   0;
    $extra_guests                   =   0;
  
    
    
  
    
    
    
    $total_price = $price_per_day;
    
    
    $wp_estate_book_down              =   floatval ( get_option('wp_estate_book_down', '') );
    $wp_estate_book_down_fixed_fee    =   floatval ( get_option('wp_estate_book_down_fixed_fee', '') );

 

    if ( !empty ( $extra_options_array ) ){
        $extra_pay_options          =      ( get_post_meta($property_id,  'extra_pay_options', true) );
     
        foreach ($extra_options_array as $key=>$value){
            if( isset($extra_pay_options[$value][0]) ){
                $extra_option_value     =   wpestate_calculate_extra_options_value($count_days,$total_guests,$extra_pay_options[$value][2],$extra_pay_options[$value][1]);
                $total_price            =   $total_price + $extra_option_value;
            }
        }
    } 
    


    if( !empty ($manual_expenses) && is_array($manual_expenses) ) {
        foreach($manual_expenses as $key=>$value){
            if(floatval($value[1]) != 0 ){
                $total_price            =   $total_price + floatval($value[1]) ;
            }
        }
    }
   
    // extra price per guest 
    if($has_guest_overload==1 && $total_extra_price_per_guest>0){
        $total_price=$total_price + $total_extra_price_per_guest;
    }
  
    
     

    //early bird discount
    ///////////////////////////////////////////////////////////////////////////
    $early_bird_discount = wpestate_early_bird($property_id,$early_bird_percent,$early_bird_days,$from_date_discount,$total_price);
   
    if($early_bird_discount>0){
        $total_price= $total_price - $early_bird_discount;
    }
    
    

    
    
    //security depozit - refundable
    ///////////////////////////////////////////////////////////////////////////
    if(intval ($security_deposit)!=0 ){
        $total_price =$total_price+$security_deposit;
    }
    
     


    
   
    
    
    $total_price_before_extra=$total_price;
   
    
    
    
      
    //cleaning or city fee per day
    ///////////////////////////////////////////////////////////////////////////

    $cleaning_fee   =   wpestate_calculate_cleaning_fee($property_id,$count_days,$curent_guest_no,$cleaning_fee,$cleaning_fee_per_day);
    $city_fee       =   wpestate_calculate_city_fee($property_id,$count_days,$curent_guest_no,$city_fee,$city_fee_per_day,$city_fee_percent,$inter_price);
    
    
    
    
    if($cleaning_fee!=0 && $cleaning_fee!=''){
        $total_price=$total_price+$cleaning_fee;
    }

    if($city_fee!=0 && $city_fee!=''){
        $total_price=$total_price+$city_fee;
    }

  
  
    
    
    if( $invoice_id == 0){
        $price_for_service_fee  =   $total_price - $security_deposit  -   floatval($city_fee)    -   floatval($cleaning_fee);
        $service_fee            =   wpestate_calculate_service_fee($price_for_service_fee,$invoice_id);
    }else{
        $service_fee  =  get_post_meta($invoice_id, 'service_fee', true);
    }
    
 
    
  
    
 
    if($include_expeses=='yes'){
        $deposit = wpestate_calculate_deposit($wp_estate_book_down,$wp_estate_book_down_fixed_fee,$total_price);
    }else{
        $deposit = wpestate_calculate_deposit($wp_estate_book_down,$wp_estate_book_down_fixed_fee,$total_price_before_extra);
    }

    
    if(intval($invoice_id)==0){
        $you_earn       =   $total_price   -   $security_deposit  -   floatval($city_fee)    -   floatval($cleaning_fee) - $service_fee;
        update_post_meta($bookid,'you_earn',$you_earn);
    }else{
        $you_earn  =    get_post_meta($bookid,'you_earn',true);
    }
   
    
      
    $taxes          =   0;
    
    if(intval($invoice_id)==0){
        $taxes_value    =   floatval(get_post_meta($property_id, 'property_taxes', true));
    }else{
        $taxes_value    =   floatval(get_post_meta($invoice_id, 'prop_taxed', true));
    }
    if($taxes_value>0){
        $taxes          =   round ( $you_earn*$taxes_value/100,2); 
    }
    
    
    if(intval($invoice_id)==0){
        update_post_meta($bookid, 'custom_price_array', $custom_price_array);
    }else{
        $custom_price_array=get_post_meta($bookid, 'custom_price_array', true);
    }
    
    $balance                                        =   $total_price - $deposit;
    $return_array=array();
    $return_array['default_price']                  =   $price_per_day;
    $return_array['week_price']                     =   $week_price;
    $return_array['month_price']                    =   $month_price;
    $return_array['total_price']                    =   $total_price;
    $return_array['inter_price']                    =   $inter_price;
    $return_array['balance']                        =   $balance;
    $return_array['deposit']                        =   $deposit;
    $return_array['from_date']                      =   $from_date;
    $return_array['to_date']                        =   $to_date;
    $return_array['cleaning_fee']                   =   $cleaning_fee;
    $return_array['city_fee']                       =   $city_fee;
    $return_array['has_custom']                     =   $has_custom;
    $return_array['custom_price_array']             =   $custom_price_array;
    $return_array['numberDays']                     =   $numberDays;
    $return_array['count_days']                     =   $count_days;
    $return_array['has_wkend_price']                =   $has_wkend_price;
    $return_array['has_guest_overload']             =   $has_guest_overload;
    $return_array['total_extra_price_per_guest']    =   $total_extra_price_per_guest;
    $return_array['extra_guests']                   =   $extra_guests;
    $return_array['extra_price_per_guest']          =   $extra_price_per_guest;
    $return_array['price_per_guest_from_one']       =   $price_per_guest_from_one;
    $return_array['curent_guest_no']                =   $curent_guest_no;
    $return_array['cover_weekend']                  =   $cover_weekend;
    $return_array['custom_period_quest']            =   $custom_period_quest;
    $return_array['security_deposit']               =   $security_deposit;
    $return_array['early_bird_discount']            =   $early_bird_discount;
    $return_array['taxes']                          =   $taxes;
    $return_array['service_fee']                    =   $service_fee;
    $return_array['youearned']                      =   $you_earn;
    return $return_array;

}


function wpestate_header_image($image){
    global $post;
    $paralax_header = get_option('wp_estate_paralax_header','');
    if( isset($post->ID)){
        
        if( is_page_template( 'splash_page.php' ) ){
            $header_type=20;
            $image =esc_html( get_option('wp_estate_splash_image','') );
            $img_full_screen                    =  'no';
            $img_full_back_type                 =   '';
            $page_header_title_over_image       =   stripslashes( esc_html ( get_option('wp_estate_splash_page_title','') ) );
            $page_header_subtitle_over_image    =   stripslashes( esc_html ( get_option('wp_estate_splash_page_subtitle','') ) ) ;
            $page_header_image_height           =   600;
            $page_header_overlay_val            =   esc_html ( get_option('wp_estate_splash_overlay_opacity','') );
            $page_header_overlay_color          =   esc_html ( get_option('wp_estate_splash_overlay_color','') );
            $wp_estate_splash_overlay_image     =   esc_html ( get_option('wp_estate_splash_overlay_image','') );
            
        }else{
            $img_full_screen                    = esc_html ( get_post_meta($post->ID, 'page_header_image_full_screen', true) );
            $img_full_back_type                 = esc_html ( get_post_meta($post->ID, 'page_header_image_back_type', true) );  
            $page_header_title_over_image       = stripslashes( esc_html ( get_post_meta($post->ID, 'page_header_title_over_image', true) ) );
            $page_header_subtitle_over_image    = stripslashes( esc_html ( get_post_meta($post->ID, 'page_header_subtitle_over_image', true) ) );
            $page_header_image_height           = floatval ( get_post_meta($post->ID, 'page_header_image_height', true) );
            $page_header_overlay_val            = esc_html ( get_post_meta($post->ID, 'page_header_overlay_val', true) );
            $page_header_overlay_color          = esc_html ( get_post_meta($post->ID, 'page_header_overlay_color', true) );
            $wp_estate_splash_overlay_image     =   '';
        }   

        if($page_header_overlay_val==''){
            $page_header_overlay_val=1;
        }
        if($page_header_image_height==0){
            $page_header_image_height=580;
        }
        
        print '<div class="wpestate_header_image full_screen_'.$img_full_screen.' parallax_effect_'.$paralax_header.'" style="background-image:url('.$image.');'; 
            if($page_header_image_height!=0){
                print ' height:'.$page_header_image_height.'px; ';
            }
            if($img_full_back_type=='contain'){
                print '  background-size: contain; ';
            }
        print '">';

        print '<div class="wpestate_header_image_right full_screen_'.$img_full_screen.' " style="background-image:url('.$image.');'; 
        print '">';

            if($page_header_overlay_color!='' || $wp_estate_splash_overlay_image!=''){
                print '<div class="wpestate_header_image_overlay" style="background-color:#'.$page_header_overlay_color.';opacity:'.$page_header_overlay_val.';background-image:url('.$wp_estate_splash_overlay_image.');"></div>';
            }

            if($page_header_title_over_image!=''){
                print '<div class="heading_over_image_wrapper" >';
                print '<h1 class="heading_over_image">'.$page_header_title_over_image.'</h1>';
                 
                if($page_header_subtitle_over_image!=''){
                    print '<div class="subheading_over_image">'.$page_header_subtitle_over_image.'</div>';
                }

                get_template_part('templates/advanced_search');                
                
                print '</div>';
            }
            
           
        print'</div>';
        
        
        
    }else{
        print '<div class="wpestate_header_image '.$post->ID.'" style="background-image:url('.$image.')"></div>';
    }
    
    
    
}