<?php

function wpestate_single_listing_pins($prop_id){
    
    $counter=0;
    $unit                       =   get_option('wp_estate_measure_sys','');
    $currency                   =   get_option('wp_estate_currency_label_main','');
    $where_currency             =   get_option('wp_estate_where_currency_symbol', '');
    $cache                      =   get_option('wp_estate_cache','');
    $place_markers=$markers     =   array();

   
      
    $args = array(
                'post_type'     =>  'estate_property',
                'p'             =>  $prop_id,
            );	

    $prop_selection = new WP_Query($args);
    wp_reset_query(); 

    $custom_advanced_search = get_option('wp_estate_custom_advanced_search','');
    $show_slider_price      = get_option('wp_estate_show_slider_price','');
    $has_slider             =   0; 

    while($prop_selection->have_posts()): $prop_selection->the_post();

        $the_id      =   get_the_ID();
        ////////////////////////////////////// gathering data for markups
        $gmap_lat    =   floatval(get_post_meta($the_id, 'property_latitude', true));
        $gmap_long   =   floatval(get_post_meta($the_id, 'property_longitude', true));

        //////////////////////////////////////  get property type
        $slug        =   array();
        $prop_type   =   array();
        $prop_city   =   array();
        $prop_area   =   array();
        $types       =   get_the_terms($the_id,'property_category' );
        $types_act   =   get_the_terms($the_id,'property_action_category' );
        $city_tax    =   get_the_terms($the_id,'property_city' );
        $area_tax    =   get_the_terms($the_id,'property_area' );




        $prop_type_name=array();
        if ( $types && ! is_wp_error( $types ) ) { 
             foreach ($types as $single_type) {
                $prop_type[]      = $single_type->slug;
                $prop_type_name[] = $single_type->name;
                $slug             = $single_type->slug;
               }

        $single_first_type= $prop_type[0];   
        $single_first_type_name= $prop_type_name[0]; 
        }else{
              $single_first_type='';
              $single_first_type_name='';
        }



        ////////////////////////////////////// get property action
        $prop_action        =   array();
        $prop_action_name   =   array();
        if ( $types_act && ! is_wp_error( $types_act ) ) { 
              foreach ($types_act as $single_type) {
                $prop_action[]      = $single_type->slug;
                $prop_action_name[] = $single_type->name;
                $slug=$single_type->slug;
               }
        $single_first_action        = $prop_action[0];
        $single_first_action_name   = $prop_action_name[0];
        }else{
            $single_first_action='';
            $single_first_action_name='';
        }


        /////////////////////////////////////////////////////////////////
       // add city
       if ( $city_tax && ! is_wp_error( $city_tax ) ) { 
               foreach ($city_tax as $single_city) {
                  $prop_city[] = $single_city->slug;
                 }

              $city= $prop_city[0];   
          }else{
                $city='';
          }

        ///////////////////////////////////////  //////////////////////// 
        //add area
         if ( $area_tax && ! is_wp_error( $area_tax ) ) { 
                 foreach ($area_tax as $single_area) {
                    $prop_area[] = $single_area->slug;
                   }

                $area= $prop_area[0];   
            }else{
                $area='';
            }     



            // composing name of the pin
            if($single_first_type=='' || $single_first_action ==''){
                  $pin                   =  sanitize_key(wpestate_limit54($single_first_type.$single_first_action));
            }else{
                  $pin                   =  sanitize_key(wpestate_limit27($single_first_type)).sanitize_key(wpestate_limit27($single_first_action));
            }
            $counter++;

            //// get price
            $clean_price    =   intval   ( get_post_meta($the_id, 'property_price', true) );
            $price          =   wpestate_show_price($the_id,$currency,$where_currency,1);
            $pin_price      =   '';
            if( get_option('wp_estate_use_price_pins_full_price','')=='no'){
                $pin_price  =   wpestate_price_pin_converter($clean_price,$where_currency,$currency);

            }
                
            $rooms          =   get_post_meta($the_id, 'property_bedrooms', true);
            $guest_no       =   get_post_meta($the_id, 'guest_no', true);  
            $size           =   get_post_meta($the_id, 'property_size', true);  		
            // if($size!=''){
            //    $size =  number_format(intval($size)) ;
            // }

            $place_markers=array();

            $place_markers[]    = rawurlencode ( get_the_title() );//0
            $place_markers[]    = $gmap_lat;//1
            $place_markers[]    = $gmap_long;//2
            $place_markers[]    = $counter;//3
            
            $post_thumbnail_id = get_post_thumbnail_id($the_id);
            $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ,'wpestate_property_listings');

            //$place_markers[]    = rawurlencode ( get_the_post_thumbnail($the_id,'wpestate_property_listings') );////4
            $place_markers[]    = rawurlencode ( $post_thumbnail_url );////4
            
            $place_markers[]    = rawurlencode ( $price );//5
            $place_markers[]    = rawurlencode ( $single_first_type );//6
            $place_markers[]    = rawurlencode ( $single_first_action );//7
            $place_markers[]    = rawurlencode ( $pin );//8
            $place_markers[]    = rawurlencode ( esc_url (get_permalink() ) );//9
            $place_markers[]    = $the_id;//10
            $place_markers[]    = rawurlencode ( $city );//11
            $place_markers[]    = rawurlencode ( $area );//12
            $place_markers[]    = $clean_price;//13
            $place_markers[]    = $rooms;//14
            $place_markers[]    = $guest_no;//15
            $place_markers[]    = $size;//16
            $place_markers[]    = rawurlencode ( $single_first_type_name );//17
            $place_markers[]    = rawurlencode ( $single_first_action_name );//18
            $property_status    = esc_html(get_post_meta($the_id, 'property_status', true) ) ;
            $property_status    = apply_filters( 'wpml_translate_single_string', $property_status, 'wpestate', 'property_status_'.$property_status );
            $place_markers[]    = rawurlencode( stripslashes ( $property_status ) );//19
            $place_markers[]    = $pin_price;


            $markers[]=$place_markers;
                  

        endwhile; 
        wp_reset_query(); 

        return json_encode($markers);


}
