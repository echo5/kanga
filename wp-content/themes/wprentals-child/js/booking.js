(function($) {
    'use strict';

    $( document ).on( 'change', '#start_date', function(){
        var endDate = new Date( $( this ).val() );
        endDate.setMonth(endDate.getMonth() + 1);
        $( '#end_date' ).val( endDate.toISOString().slice(0,10) );
        $( '#end_date' ).val( endDate.toISOString().slice(0,10) );
        $( 'input[name="booking_guest_no"]' ).val('1');
        $( '#booking_guest_no_wrapper' ).attr('data-value', '1');
    });

    $( document ).on( 'change', '#booking_from_date', function(e) {
        var endDate = new Date( $( this ).val() );
        endDate.setMonth(endDate.getMonth() + 1);
        $( '#booking_to_date' ).val( endDate.toISOString().slice(0,10) );
        $( '#booking_to_date' ).val( endDate.toISOString().slice(0,10) );
        $( '#booking_guest_no' ).val('1');
        $( '#booking_guest_no_wrapper' ).attr('data-value', '1');
    });

})(jQuery);