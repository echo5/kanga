<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style('wpestate_bootstrap',get_template_directory_uri().'/css/bootstrap.css', array(), '1.0', 'all');
        wp_enqueue_style('wpestate_bootstrap-theme',get_template_directory_uri().'/css/bootstrap-theme.css', array(), '1.0', 'all');
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' ); 
        wp_enqueue_style('wpestate_media',get_template_directory_uri().'/css/my_media.css', array(), '1.0', 'all'); 
        wp_dequeue_script( 'wpestate_mapfunctions' );
        wp_enqueue_script( 'wpestate_mapfunctions', get_stylesheet_directory_uri().'/js/mapfunctions.js', array('jquery') );
        wp_enqueue_script( 'child_booking', get_stylesheet_directory_uri().'/js/booking.js', array('jquery') );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

function custom_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/login/login-styles.css' );
}
// END ENQUEUE PARENT ACTION

require_once 'includes/property.php';
require_once 'includes/listing_functions.php';
require_once 'includes/ajax_functions.php';
require_once 'includes/help_functions.php';
require_once 'includes/pin_management.php';