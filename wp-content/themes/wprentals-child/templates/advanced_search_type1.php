<?php 
global $post;
$adv_search_what            =   get_option('wp_estate_adv_search_what','');
$show_adv_search_visible    =   get_option('wp_estate_show_adv_search_visible','');
$close_class                =   '';

if($show_adv_search_visible=='no'){
    $close_class='adv-search-1-close';
}

if(isset( $post->ID)){
    $post_id = $post->ID;
}else{
    $post_id = '';
}

$args = wpestate_get_select_arguments();
$allowed_html = array();
$allowed_html_list =    array('li' => array(
                                        'data-value'        =>array(),
                                        'role'              => array(),
                                        'data-parentcity'   =>array(),
                                        'data-value2'       =>array()
                        ) );

$extended_search    =   get_option('wp_estate_show_adv_search_extended','');
$extended_class     =   '';
$action_select_list =   wpestate_get_action_select_list($args);
$categ_select_list  =   wpestate_get_category_select_list($args);

if ( $extended_search =='yes' ){
    $extended_class='adv_extended_class';
    if($show_adv_search_visible=='no'){
        $close_class='adv-search-1-close-extended';
    }      
}
    
$header_type                =   get_post_meta ( $post->ID, 'header_type', true);
$global_header_type         =   get_option('wp_estate_header_type','');

$google_map_lower_class='';
 if (!$header_type==0){  // is not global settings
    if ($header_type==5){ 
        $google_map_lower_class='adv_lower_class';
    }
}else{    // we don't have particular settings - applt global header          
    if($global_header_type==4){
        $google_map_lower_class='adv_lower_class';
    }
} // end if header
    

    
    
?>

 
 <div class="adv-1-wrapper"> 
    </div>  

<div class="adv-search-1 <?php echo $google_map_lower_class.' '.$close_class.' '.$extended_class;?>" id="adv-search-1" data-postid="<?php echo $post_id; ?>"> 

  
    <form  method="get"  id="main_search" action="<?php print $adv_submit; ?>" >
        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
        <div class="col-md-8 map_icon">    
            <?php 
            $show_adv_search_general            =   get_option('wp_estate_wpestate_autocomplete','');
            $wpestate_internal_search           =   '';
            if($show_adv_search_general=='no'){
                $wpestate_internal_search='_autointernal';
                print '<input type="hidden" class="stype" id="stype" name="stype" value="tax">';
            }
            ?>
            <input type="text"    id="search_location<?php echo $wpestate_internal_search;?>"      class="form-control" name="search_location" placeholder="<?php esc_html_e('Where do you want to go ?','wpestate');?>" value="" >              
            <input type="hidden" id="advanced_city"      class="form-control" name="advanced_city" data-value=""   value="" >              
            <input type="hidden" id="advanced_area"      class="form-control" name="advanced_area"   data-value="" value="" >              
            <input type="hidden" id="advanced_country"   class="form-control" name="advanced_country"   data-value="" value="" >              
            <input type="hidden" id="property_admin_area" name="property_admin_area" value="">
     
            
        </div>
        <div style="display:none;" id="searchmap"></div>
        
        <!-- <div class="col-md-3">
            <div class="dropdown form-control types_icon" id="categ_list" >
                <div data-toggle="dropdown" id="adv_categ" class="filter_menu_trigger" data-value="all"> <?php //esc_html_e('All Types','wpestate');?> <span class="caret caret_filter"></span> </div>           
                <input type="hidden" name="filter_search_type[]" value="">
                <ul  class="dropdown-menu filter_menu" role="menu" aria-labelledby="adv_categ">
                    <?php  //print wp_kses($categ_select_list,$allowed_html_list); ?>
                </ul>        
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="dropdown form-control actions_icon">
                <div data-toggle="dropdown" id="adv_actions" class="filter_menu_trigger" data-value="all"> <?php //esc_html_e('All Sizes','wpestate');?> <span class="caret caret_filter"></span> </div>           
                <input type="hidden" name="filter_search_action[]" value="">
                <ul  class="dropdown-menu filter_menu" role="menu" aria-labelledby="adv_actions">
                    <?php //print wp_kses($action_select_list,$allowed_html_list);?>
                </ul>        
            </div>
        </div> -->
        
        <div class="col-md-3">
        <input name="submit" type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="advanced_submit_2" value="<?php esc_html_e('Search','wpestate');?>">
        </div>

        <p class="learn-more">
            <a href="<?php echo home_url( '/host'); ?>"><?php _e( 'Learn how to rent your space', 'kanga' ); ?> <i class="fa fa-long-arrow-right"></i></a>
        </p>
              
        <div id="results">
            <?php esc_html_e('We found ','wpestate')?> <span id="results_no">0</span> <?php esc_html_e('results.','wpestate'); ?>  
            <span id="showinpage"> <?php esc_html_e('Do you want to load the results now ?','wpestate');?> </span>
        </div>

    </form>   

</div>  
<?php get_template_part('libs/internal_autocomplete_wpestate'); ?>