<?php
global $post;
global $property_action_terms_icon;
global $property_action;
global $property_category_terms_icon;
global $property_category;
global $guests;
global $bedrooms;
global $bathrooms;
global $favorite_text;
global $favorite_class;
global $options;
$currency = esc_html( get_option('wp_estate_currency_label_main', '') );
$price = intval ( get_post_meta($post->ID, 'property_price', true) );
$service_fee = floatval ( get_option('wp_estate_service_fee','') );

?>--><div class="property_header property_header2">
        <div class="property_categs ">
            
            <div class="property_header_wrapper 
                <?php 
                if ( $options['content_class']=='col-md-12' || $options['content_class']=='none'){
                    print 'col-md-8';
                }else{
                   print  $options['content_class']; 
                }?> 
            ">
            
                <div class="category_wrapper ">
                    <div class="category_details_wrapper">
                         <?php if( $property_action!='') {
                            echo $property_action; ?> <span class="property_header_separator">|</span>
                            <div class="schema_div_noshow"  itemprop="actionStatus"><?php echo strip_tags($property_action); ?></div>
                        <?php } ?>

                        <?php  if( $property_category!='') {
                            echo $property_category;?> <span class="property_header_separator">|</span>
                            <div class="schema_div_noshow"  itemprop="additionalType"><?php echo strip_tags($property_category); ?></div>
                        <?php } ?> 
                            <?php  
                            if($guests==1){
                                print '<span class="no_link_details">'.$guests.' '. esc_html__( 'Guest','wpestate').'</span>';
                            }else{
                                print '<span class="no_link_details">'.$guests.' '. esc_html__( 'Guests','wpestate').'</span>';
                            }    
                            ?><span class="property_header_separator">|</span>

                            <?php  
                            if($bedrooms==1){
                                print  '<span class="no_link_details">'.$bedrooms.' '.esc_html__( 'Bedroom','wpestate').'</span>';
                            }else{
                                print  '<span class="no_link_details">'.$bedrooms.' '.esc_html__( 'Bedrooms','wpestate').'</span>';
                            }
                            ?><span class="property_header_separator">|</span>

                            <?php 
                            if($bathrooms==1){
                                print  '<span class="no_link_details">'.$bathrooms.' '.esc_html__( 'Bath','wpestate').'</span>';
                            }else{
                                print  '<span class="no_link_details">'.$bathrooms.' '.esc_html__( 'Baths','wpestate').'</span>';
                            }
                            ?>
                    </div>
                    
                    <a href="#listing_calendar" class="check_avalability"><?php esc_html_e('Check Availability','wpestate');?></a>
                </div>
                
              
                
                <div  id="listing_description">
                <?php
                    $content = get_the_content();
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    $property_description_text =  get_option('wp_estate_property_description_text');
                    if (function_exists('icl_translate') ){
                        $property_description_text     =   icl_translate('wpestate','wp_estate_property_description_text', esc_html( get_option('wp_estate_property_description_text') ) );
                    }
                    
                    if($content!=''){   
                        print '<h4 class="panel-title-description">'.$property_description_text.'</h4>';
                        print '<div itemprop="description" class="panel-body">'.$content.'</div>';       
                    }
                ?>
                </div>
                
                
               
               
                <div id="view_more_desc"><?php esc_html_e('View more','wpestate');?></div>
            
        </div>
    <?php  
        $post_id=$post->ID; 
        $guest_no_prop ='';
        if(isset($_GET['guest_no_prop'])){
            $guest_no_prop = intval($_GET['guest_no_prop']);
        }
        $guest_list= wpestate_get_guest_dropdown('noany');
    ?>
    
                
                
                
    
    <div class="booking_form_request  
        <?php
        if($options['sidebar_class']=='' || $options['sidebar_class']=='none' ){
            print ' col-md-4 '; 
        }else{
            print $options['sidebar_class'];
        }
        ?>
         " id="booking_form_request">
        <div id="booking_form_request_mess"></div>
            <h3 ><?php esc_html_e('Book Now','wpestate');?></h3>
            <h2><?php echo $currency . number_format( $price, 2 ); ?><?php _e( '/month', 'wpestate' ); ?></h2>
            <p>
                <?php _e( 'Service Fee: ', 'wpestate' ); ?>
                <?php echo $currency . number_format( $price * $service_fee / 100, 2 ); ?>
                <span class="tooltip-wrap">
                    <img class="tooltip-trigger" src="<?php echo get_stylesheet_directory_uri() . '/img/admin/help.png'; ?>" />
                    <span class="tooltip-box"><?php _e( 'This is what helps Kanga provide this awesome service.', 'wpestate' ); ?></span>
                </span>
            </p>
            <hr>
            <p><strong><?php _e( 'Your total', 'wpestate' ); ?> <?php echo $currency . number_format( $price * $service_fee / 100 + $price, 2 ); ?></strong> <?php _e( '/month', 'wpestate' ); ?></p>
             
                <div class="has_calendar calendar_icon">
                    <input type="text" id="start_date" placeholder="<?php esc_html_e('Check in','wpestate'); ?>"  class="form-control calendar_icon" size="40" name="start_date" 
                            value="<?php if( isset($_GET['check_in_prop']) ){
                               echo sanitize_text_field ( $_GET['check_in_prop'] );
                            }
                            ?>">
                </div>

                <div class=" has_calendar calendar_icon">
                    <input type="text" id="end_date" disabled placeholder="<?php esc_html_e('Check Out','wpestate'); ?>" class="form-control calendar_icon" size="40" name="end_date" 
                            value="<?php if( isset($_GET['check_out_prop']) ){
                               echo sanitize_text_field ( $_GET['check_out_prop'] );
                            }
                            ?>">
                </div>

                <div class=" has_calendar guest_icon ">
                    <?php 
                    $max_guest = get_post_meta($post_id,'guest_no',true);
                    print '
                    <div class="dropdown form-control">
                        <div data-toggle="dropdown" id="booking_guest_no_wrapper" class="filter_menu_trigger" data-value="';
                            if(isset($_GET['guest_no_prop']) && $_GET['guest_no_prop']!=''){
                                echo esc_html( $_GET['guest_no_prop'] );
                            }else{
                              echo 'all';
                            }
                        print '">';
                        print '<div class="text_selection">';
                        if(isset($_GET['guest_no_prop']) && $_GET['guest_no_prop']!=''){
                            echo esc_html( $_GET['guest_no_prop'] ).' '.esc_html__( 'guests','wpestate');
                        }else{
                            esc_html_e('Guests','wpestate');
                        }
                        print '</div>';
                        
                        print '<span class="caret caret_filter"></span>
                        </div>           
                        <input type="hidden" name="booking_guest_no"  value="">
                        <ul  class="dropdown-menu filter_menu" role="menu" aria-labelledby="booking_guest_no_wrapper" id="booking_guest_no_wrapper_list">
                            '.$guest_list.'
                        </ul>        
                    </div>';
                    ?> 
                </div>
            

                
                <?php
                // shw extra options
                wpestate_show_extra_options_booking($post_id)
                ?>
            
                <!-- <p class="full_form " id="add_costs_here"></p>             -->

                <input type="hidden" id="listing_edit" name="listing_edit" value="<?php echo $post_id;?>" />

                <div class="submit_booking_front_wrapper">
                    <?php   
                    $overload_guest                 =   floatval   ( get_post_meta($post_id, 'overload_guest', true) );
                    $price_per_guest_from_one       =   floatval   ( get_post_meta($post_id, 'price_per_guest_from_one', true) );
                    ?>
                    
                     <?php  $instant_booking                 =   floatval   ( get_post_meta($post_id, 'instant_booking', true) ); 
                    if($instant_booking ==1){ ?>
                        <div id="submit_booking_front_instant_wrap"><input type="submit" id="submit_booking_front_instant" data-maxguest="<?php echo $max_guest; ?>" data-overload="<?php echo $overload_guest;?>" data-guestfromone="<?php echo $price_per_guest_from_one; ?>"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" value=" <?php esc_html_e('Instant Booking','wpestate');?>" /></div>
                    <?php }else{?>   
                        <input type="submit" id="submit_booking_front" data-maxguest="<?php echo $max_guest; ?>" data-overload="<?php echo $overload_guest;?>" data-guestfromone="<?php echo $price_per_guest_from_one; ?>"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" value="<?php esc_html_e('Book Now','wpestate');?>" />
                    <?php }?>
                        
                    <?php wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' );?>
                </div>

                <p class="no-charge-yet"><?php _e( 'YOU WON\'T BE CHARGED YET', 'wpestate' ); ?></p>

                <div class="third-form-wrapper">
                    <div class="col-md-6 reservation_buttons">
                        <div id="contact_host" class="col-md-6"  data-postid="<?php the_ID();?>">
                            <?php esc_html_e('Contact Owner','wpestate');?>
                        </div>  
                    </div>
                    <div class="col-md-6 reservation_buttons">
                        <a href="<?php echo home_url( '/faq' ); ?>" id="faq-btn">
                            <?php _e( 'FAQ', 'wpestate' ); ?>
                        </a>                 
                    </div>
                </div>
                        
                <p class="renters-insurance"><?php _e( 'RENTERS INSURANCE AVAILABLE UPON REQUEST', 'wpestate' ); ?></p>
   

        </div>
    
    
    
    
     </div>
</div>